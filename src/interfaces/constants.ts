interface Constants {
  templates: {
    excerpt: string,
    recentPost: string,
    excerptTag: string,
    postDetail: string,
    about: string,
    archive: string,
    archiveLink: string,
    tags: string,
    tagItem: string,
    tagPosts: string,
    postTag: string,
  },
  api: {
    url: string,
    posts: string,
  },
  TAGS: string
};

export default Constants;